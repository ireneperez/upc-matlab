function y=nor(x)
%Funci� que normalitza sense signe
format long e
f=size(x,1);
i=1;
y=[];

while i<=f
	den=abs(x(i,1));
	m=(1/den).*x(i,:);
	y=[y;m];
	i=i+1;
end

