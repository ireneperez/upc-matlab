clear all;
clc
%load p01r2bhatnou.mat %per pitch (22012018)
%load Feina5gener %per pitch
load r01r1bhatnou.mat %per roll
%load y01r2bhat.mat %per yaw
nb=4;na=4;
vector=1:1:15;
H=Hdata;
%w=2*pi.*f;
w=w(vector);
maximHR=max(real(H(vector)));minimHR=min(real(H(vector)));maximHI=max(imag(H(vector)));minimHI=min(imag(H(vector)));
%Aminori=Amin(2,:);Amaxori=Amax(1,:);Bminori=Bmin(2,:);Bmaxori=Bmax(1,:);
lb=[-Inf,-Inf,-Inf,-Inf,-Inf,-Inf,-Inf,-Inf,1,-Inf,-Inf,-Inf,1,-Inf,-Inf,-Inf];
ub=[Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,1,Inf,Inf,Inf,1,Inf,Inf,Inf];
lb=[-Inf,-Inf,-Inf,-Inf,-Inf,-Inf,-Inf,-Inf,-Inf,-Inf,-Inf,-Inf,-Inf,-Inf,-Inf,-Inf];
ub=[Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf];
options=optimset('Display','iter');
warning off
Bmax=N;
Amax=D;
Bmin=n;
Amin=d;
tic
x = fmincon(@fcost,[Bmin Bmax Amin Amax],[],[],[],[],lb,ub,@restriccions,options,maximHR,minimHR,maximHI,minimHI,w,nb,na)
% x = fmincon(@fcost,[(lb(1:3)+ub(1:3))./2 (lb(1:3)+ub(1:3))./2 (lb(4:end)+ub(4:end))./2 (lb(4:end)+ub(4:end))./1]...
%     ,[],[],[],[],lb,ub,@restriccions,options,H,w,nb,na);
toc