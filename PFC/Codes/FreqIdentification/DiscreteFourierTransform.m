function [RealPart,ImagPart] = DiscreteFourierTransform(data,f,Ts,fast,windowing,method);
%   
%   Discrete Fourier Transform (DFT) or Fast Fourier Transform (FFT)
%   [RealPart,ImagPart] = DiscreteFourierTransform(data,f,Ts,fast,windowing,method);
%
%   Input arguments:
%   data: time data to be transformed
%   f: set of frequencies in Hertzs
%   Ts: sample time in seconds
%   fast: 1 -> FFT is applied
%         0 -> DFT is applied
%   windowing: 1 -> windowing is applied on time domain data
%   	       0 -> rectangular window (no window applied on time domain data)
%   method:    the type of the window when necessary
%    'bartlett'       - Bartlett window.
%    'barthannwin'    - Modified Bartlett-Hanning window. 
%    'blackman'       - Blackman window.
%    'blackmanharris' - Minimum 4-term Blackman-Harris window.
%    'bohmanwin'      - Bohman window.
%    'chebwin'        - Chebyshev window.
%    'flattopwin'     - Flat Top window.
%    'gausswin'       - Gaussian window.
%    'hamming'        - Hamming window.
%    'hann'           - Hann window.
%    'kaiser'         - Kaiser window.
%    'nuttallwin'     - Nuttall defined minimum 4-term Blackman-Harris window.
%    'parzenwin'      - Parzen (de la Valle-Poussin) window.
%    'rectwin'        - Rectangular window.
%    'taylorwin'      - Taylor window.
%    'tukeywin'       - Tukey window.
%    'triang'         - Triangular window.
%     NOTE: for more information on windows see "On the use of windows for harmonic analysis with the DFT" by FREDRIC J. HARRIS
%
%   Output arguments:
%   RealPart: real part of the harmonic at each frequency
%   ImagPart: imaginary part of the harmonic at each frequency
% 
%   The equation for the FFT is the MATLAB fft.m function itself.
%   The equation for the DFT is the following:
%               n=N-1
%   x[f]=2/N *  sum(x[n]*exp(-j*2*pi*n*f*Ts))
%               n=0 
%   FYI:
%   Euler -> exp(-ja)= cos a - j sen a
%   Maximum frequency = Nyquist Frequency = (N/2+1)/(Ts*N)

format long

N=length(data);

if max(int16(f*Ts*N+1))>N/2
   disp('Hi ha almenys una frequencia mes gran que la frequencia de Nyquist')

else
    if windowing~=0
        if iscolumn(data)
            wd=window(method,N);
            data=data.*wd;
        else 
            wd=window(method,N)';
            data=data.*wd;
        end   
    end
    
    if fast==0
        %s'aplica la dft (Transformada Discreta de Fourier)
        w=2*pi*f;
        z=zeros(length(w),1);
        for j = 1:length(w)
            for n = 1:N
                z(j)=z(j)+data(n)*exp(-i*w(j)*(n-1)*Ts);       
            end
        end
        z=z.*2/N;
    else
        %s'aplica la fft (Transformada Rapida de Fourier)
        %n=int16(f*Ts*N+1);  %posicions en el vector de sortida de la fft corresponent a les
        n=round(f*Ts*N+1);  %posicions en el vector de sortida de la fft corresponent a les

                            %frequencies demanades (corresponent a frequencies negatives per
                            %tant, s'ha de canviar el signe de la fase)

        z=fft(data)';
        z=z(n)*2/N;       %dades utils (dades corresponents a les frequencies demanades)
    end
        RealPart=real(z);
        ImagPart=imag(z);
end
