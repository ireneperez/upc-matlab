clear all;
what=1;
na=2;nb=2;
if what==1 %11
    load 'p01r1.mat';
    velo = 5; tau = .2;index=1;    
    NumControlAprox=poly([2.52+j*5.38 2.52-j*5.38 0.0646])./2.630267991895381e+04;
elseif what==2 %22
    load 'r01r2.mat';
    velo = 6; tau = .2;index=2;
    NumControlAprox=poly([1.52+j*0.634 +1.52-j*0.634])./3.890451449942805e+03;%3.758374042884443e+04;
elseif what==3 %12
    load 'r01r2.mat';
    velo = 5; tau=2;index=2;
    NumControlAprox=poly([1.26+j*1.77 +1.26-j*1.77])./2.187761623949552e+04;
elseif what==4 %21
    load 'p01r4.mat';
    velo = 6;  tau=2;  index=1;
    NumControlAprox=poly([+1.44+j*1.67 +1.44-j*1.67])./2.070395782768865e+04;
elseif what==5 %33
    load 'y01r2.mat';
    velo = 3; tau = .2;index=3;
    NumControlAprox=-1*poly([0.275+j*0.948 0.275-j*0.948])./26.242185433844419;
end
tall=200;
data=matriu(:,index);
data=data';
Angle=data(tall:length(navigation(:,velo)));
V=abs(navigation(tall:end,velo));
V = V';
%z=[V' Angle'];
%idplot(z)
f=linspace(.1,2,15);
w=2*3.1416.*f;
[EReal,EImag] = DiscreteFourierTransform(Angle,f,.1,1,1,'blackmanharris');
Fe=EReal+sqrt(-1).*EImag;
%subplot(2,1,2)
%bar(f,abs(Fe))
%ylabel('consigna pitch (????)')
[SReal,SImag] = DiscreteFourierTransform(V,f,.1,1,1,'blackmanharris');
Fs=SReal+sqrt(-1).*SImag;
%subplot(2,1,1)
%bar(f,abs(Fs))
%ylabel('Vx (mm/seg)')
H=Fs./Fe; %Dades Frequencials

%initials
Wt=ones(1,length(H));
[Bini,Aini] = invfreqs(H,w,nb,na,Wt);
%
[ H,B,A,w,pesos ] = identifica(H,f,na,nb,Wt );
%
[m,a]=bode(B,A,w);
[m2,a2]=bode(Bini,Aini,w);
Sys=tf(B,A);
close all
figure
subplot(2,1,1)
hold on
semilogx(20*log10(m),'b--')
%semilogx(m2,'k')
semilogx(20*log10(abs(H)),'r')
grid
title('Mòdul');
ylabel('dB');
legend('Ajust','Dades')
%legend('Final','Inicial','Dades');
subplot(2,1,2)
hold on
semilogx(a+[0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]','b--')
%semilogx(a2+[360 360 360 360 360 0 0 0 0 0 0 0 0 0 0]','k')
semilogx(180/pi*angle(H),'r')
grid
title('Angle');
ylabel('degrees');
legend('Ajust','Dades')
%legend('Inicial','Final','Dades');
figure
semilogy(pesos)
title('Convergència dels pesos Wt');
ylabel('1/(D·A)')
grid
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
a(:) = (pi/180)*a(:);
modelG = m(:).*exp((sqrt(-1))*a(:));
Cost = (abs(H(:)-modelG(:))).^2;Cost=sum(Cost);
d = na+nb+1; N=length(H);
Akaike = Cost*((1+(d/N))/(1-(d/N)));
%%%%%%%%%%%%%%%%%%%%%%%% Càlcul del controlador %%%%%%%%%%
Ndesitjat=1;
Ddesitjat=[2 1];
NumControl=conv(A,1);
DenControl=conv(B,[tau 0]);
if what==5
    arrels=roots(DenControl);
    arrels(3)=-arrels(3);
    DenControl2=poly(arrels);
    DenControl=DenControl2.*(DenControl(end-1)./DenControl2(end-1));
end
figure 
bode(NumControl,DenControl)
hold on
%DenControlAprox=[1/11.6 1 0];
DenControlAprox2=[1 0];
NumControlAprox2=[1];
bode(NumControlAprox,DenControlAprox2)
%bode(NumControlAprox2,DenControlAprox2)
legend('C','Caprox','Caprox2');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% close all;
% controlador = tf(fliplr(NumControlAprox),[1 0]);
% model = feedback(controlador*Sys,+1);
% step(model);
% xlabel('Temps (s)'); ylabel('Velocitat lineal (mm/s)');
% title('Model amb controlador en llaç tancat');
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%SIMULINK$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$4
% if what==1
%     G_11num = fliplr(B);
%     G_11den = fliplr(A);
%     R_11num = fliplr(NumControl);
%     R_11den = fliplr(DenControl(1:end-1));R_11den=horzcat(R_11den,0);
%     save('datapitch.mat','G_11num','G_11den','R_11num','R_11den');
% elseif what==2
%     G_22num = fliplr(B);
%     G_22den = fliplr(A);
%     R_22num = fliplr(NumControl);
%     %R_22num = NumControlAprox;
%     R_22den = fliplr(DenControl(1:3));R_22den=horzcat(R_22den,0);
%     save('dataroll.mat','G_22num','G_22den','R_22num','R_22den');
% elseif what==3
%     G_12num = fliplr(B);
%     G_12den = fliplr(A);
%     load('datapitch.mat');
%     R_12num = -conv(G_12num,G_11den);
%     R_12den = conv(G_12den,G_11num);
%     save('data12.mat','G_12num','G_12den','R_12num','R_12den');
% elseif what==4
%     G_21num = fliplr(B);
%     G_21den = fliplr(A);
%     load('dataroll.mat');
%     R_21num = -conv(G_21num,G_22den);
%     R_21den = conv(G_21den,G_22num);
%     save('data21.mat','G_21num','G_21den','R_21num','R_21den');
% elseif what==5
%     G_33num = B;
%     G_33den = A;
%     %R_33num = fliplr(NumControl);
%     R_33num = NumControlAprox;
%     R_33den = fliplr(DenControl(1:3));R_33den=horzcat(R_33den,0);
%     save('datayaw.mat','G_33num','G_33den','R_33num','R_33den');
% end