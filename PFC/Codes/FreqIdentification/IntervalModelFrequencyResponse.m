function [MaxReal,MinReal,MaxImag,MinImag,ModuleMax,ModuleMin,AngleMax,AngleMin]=IntervalModelFrequencyResponse(Hdata,n,N,d,D,w)
% 	USE: [MaxReal,MinReal,MaxImag,MinImag,ModuleMax,ModuleMin,AngleMax,AngleMin]=IntervalModelFrequencyResponse(H,n,N,d,D,w)
% 	NEEDS: atan2dmod.m (user) and fmincon.m (opt. toolbox)
warning off
MaxReal=zeros(1,length(w));
MaxImag=zeros(1,length(w));
MinReal=zeros(1,length(w));
MinImag=zeros(1,length(w));
ModuleMax=zeros(1,length(w));
ModuleMin=zeros(1,length(w));
AngleMax=zeros(1,length(w));
AngleMin=zeros(1,length(w));
Norder=length(N)-1;
Dorder=length(D)-1;
for i=1:1:length(w)
%     [N1,N2,N3,N4]=kharitonov(n,N);
%     [D1,D2,D3,D4]=kharitonov(d,D);
    
    Maximise=1;
    [x,MaxRealValue] = fmincon(@fcostreal,[(n+N)./2,(d+D)./2],[],[],[],[],[n,1,d(2:end)],[N,1,D(2:end)],[],[],Maximise,Norder,w(i));
    Maximise=0;
    [x,MinRealValue] = fmincon(@fcostreal,[(n+N)./2,(d+D)./2],[],[],[],[],[n,1,d(2:end)],[N,1,D(2:end)],[],[],Maximise,Norder,w(i));
    Maximise=1;
    [x,MaxImagValue] = fmincon(@fcostimag,[(n+N)./2,(d+D)./2],[],[],[],[],[n,1,d(2:end)],[N,1,D(2:end)],[],[],Maximise,Norder,w(i));
    Maximise=0;
    [x,MinImagValue] = fmincon(@fcostimag,[(n+N)./2,(d+D)./2],[],[],[],[],[n,1,d(2:end)],[N,1,D(2:end)],[],[],Maximise,Norder,w(i));
    if isempty(MaxRealValue)
        MaxRealValue=Inf;
    else
    MaxReal(i)=-MaxRealValue;% sign change in order to undo the maximisation procedure (-minimisation procedure)
    end
    if isempty(MaxImagValue)
        MaxImagValue=Inf;
    else
    MaxImag(i)=-MaxImagValue;% sign change in order to undo the maximisation procedure (-minimisation procedure)
    end
    if isempty(MinRealValue)
        MinRealValue=0;
    else
    MinReal(i)=MinRealValue;
    end
    if isempty(MinImagValue)
        MinImagValue=0;
    else
        MinImag(i)=MinImagValue;
    end
end
Cdata=[MaxReal+MaxImag.*sqrt(-1);MinReal+MaxImag.*sqrt(-1);MinReal+MinImag.*sqrt(-1);MaxReal+MinImag.*sqrt(-1)];
for i=1:size(Cdata,2)
   ModuleMax(i)=max(abs(Cdata(:,i))); 
   ModuleMin(i)=min(abs(Cdata(:,i)));
   AngleMax(i)=max(atan2dmod(imag(Cdata(:,i)),real(Cdata(:,i))));
   AngleMin(i)=min(atan2dmod(imag(Cdata(:,i)),real(Cdata(:,i))));
end

load 'r01r1bhatnou.mat'
H = Hdata;
[m,a]=bode(B0,A0,w);
[rCdata,RCdata,iCdata,ICdata]=GeneraRectanglesDades(Cdata,1);
figure
subplot(2,1,1)
semilogx(w,20*log10(m),'b--')
hold on
semilogx(w,20.*log10(ModuleMax),'-sr','MarkerSize',10,...
   'MarkerEdgeColor','black',...
   'MarkerFaceColor',[1 .6 .6])
hold on
semilogx(w,20.*log10(ModuleMin),'-sb','MarkerSize',10,...
   'MarkerEdgeColor','black',...
   'MarkerFaceColor',[.6 .6 1])
hold on
semilogx(w,20*log10(abs(H)),'r')
grid minor
xlabel('Pulsació en rad/seg')
ylabel('Guany en dB')
legend('Model nominal','Model intervalar','Model intervalar','Dades')
subplot(2,1,2)
semilogx(w,a','b--')
hold on
semilogx(w,AngleMax,'-sr','MarkerSize',10,...
   'MarkerEdgeColor','black',...
   'MarkerFaceColor',[1 .6 .6])
hold on
semilogx(w,AngleMin,'-sb','MarkerSize',10,...
   'MarkerEdgeColor','black',...
   'MarkerFaceColor',[.6 .6 1])
hold on
caca=180/pi*angle(H)-[0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]';
semilogx(w,caca,'r')
xlabel('Pulsació en rad/seg')
ylabel('Angle en ^o')
legend('Model nominal','Model intervalar','Model intervalar','Dades')
grid minor