clear all;
clc
% load FeinaFetaFme29nov2017
load 19122017
load negre19122017
vector=1:1:15;
w=w(vector);
maximHR=maximHR(vector);minimHR=minimHR(vector);maximHI=maximHI(vector);minimHI=minimHI(vector);
%Aminori=Amin(2,:);Amaxori=Amax(1,:);Bminori=Bmin(2,:);Bmaxori=Bmax(1,:);
lb=[-Inf,-Inf,-Inf,-Inf,-Inf,-Inf,-Inf,-Inf,1,-Inf,-Inf,-Inf,1,-Inf,-Inf,-Inf];
ub=[Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,1,Inf,Inf,Inf,1,Inf,Inf,Inf];
lb=[-Inf,-Inf,-Inf,-Inf,-Inf,-Inf,-Inf,-Inf,-Inf,-Inf,-Inf,-Inf,-Inf,-Inf,-Inf,-Inf];
ub=[Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf,Inf];
options=optimset('Display','iter');
warning off
Bmax=Bmax.*[1.2 1.2 1.2 .8];
Amax=Amax.*[1.2 .8 1.2 .8];
Bmin=Bmin.*[.8 .8 .8  1.2];
Amin=Amin.*[.8 1.2 .8 1.2];

tic
x = fmincon(@fcost,[Bmin Bmax Amin Amax],[],[],[],[],lb,ub,@restriccions,options,maximHR,minimHR,maximHI,minimHI,w,nb,na)
% x = fmincon(@fcost,[(lb(1:3)+ub(1:3))./2 (lb(1:3)+ub(1:3))./2 (lb(4:end)+ub(4:end))./2 (lb(4:end)+ub(4:end))./1]...
%     ,[],[],[],[],lb,ub,@restriccions,options,H,w,nb,na);
toc