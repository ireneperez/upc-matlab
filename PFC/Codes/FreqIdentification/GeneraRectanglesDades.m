function [rCdata,RCdata,iCdata,ICdata]=GeneraRectanglesDades(Cdata,dibuix)
% [rCdata,RCdata,iCdata,ICdata]=GeneraRectanglesDades(Cdata,dibuix)
% Cdata és un conjunt de dades empíriques en domini freqüència en diferents files.
% El que fa la funció és representar (si dibuix==1) les dades 
% i els rectangles que les contenen en el pla de Nyquist.
% Si dibuix=0 no es dibuixa res a la pantalla.
rCdata=min(real(Cdata));
RCdata=max(real(Cdata));
iCdata=min(imag(Cdata));
ICdata=max(imag(Cdata));
if dibuix
    figure
    plot(real(Cdata),imag(Cdata),'d')
    axis square
    xlabel('Eix real')
    ylabel('Eix imaginari')
    hold on
    for i=1:size(Cdata,2)
    plot([rCdata(i) RCdata(i) RCdata(i) rCdata(i) rCdata(i)],[ICdata(i) ICdata(i) iCdata(i) iCdata(i) ICdata(i)],'k')
    end
    grid minor
end

