function [n,N,d,D,B0,A0] = bhattacharyya2(Hdata,w,Wt,nb,na)
% % bhattacharyya.m is a function that finds the minimum and maximum num and den polynomials.
% % It needs nominalIDw.m to work properly.
% % Usage: [n,N,d,D] = bhattacharyya(Hdata,w,Wt,nb,na)
% % where:
% % Hdata is the complex data to be fitted by the model
% % w is the angular frequency array in rad/sec
% % weights i the weight array in order to emphasize the fit on the desired frequencies.
% % nb and na are the num and den polynomial orders

% % % Definitions
E=zeros(na+nb+2,length(w));
Baux=zeros(1,nb+1);
B=zeros(1,nb+1);
Aaux=zeros(1,na+1);
A=zeros(1,na+1);
Bmatrix=zeros(length(w),length(B));
Amatrix=zeros(length(w),length(A));
Bdiff=Bmatrix;
Adiff=Amatrix;
generalA=zeros(2,na+nb+2);
generalB=[0 0]';
jw=sqrt(-1).*w;
f=zeros(size(w));
f = w./(2*pi);
Hdata=Hdata.';
% % % nominal ID and nominal model FR
[ B0,A0,weights ] = nominalIDw( Hdata,f,nb,na,Wt);
Hmodel = freqresp(tf(B0,A0),w);
Hmodel = squeeze(Hmodel);
Hmodel=Hmodel.';
Haux=zeros(size(Hdata));
Wt=weights(end,:);
% % % sensitivity array and matrix
for i=1:length(w)
    Haux=Hmodel;
    Haux(i)=Hdata(i);
    [ B,A,weights ] = nominalIDw( Haux,f,nb,na,Wt);
    Wt=weights(end,:);
    Bmatrix(i,:)=B;
    Amatrix(i,:)=A;
    Bdiff(i,:)=abs(B0-B);
    Adiff(i,:)=abs(A0-A);
end
sensArray=[fliplr(mean(Adiff)) fliplr(mean(Bdiff))];
sensMatrix=diag(sensArray);
m11=[1 -1 -1 1];
m12=[-1 0 1 0];
m21=[1 1 -1 -1];
m22=[0 1 0 -1];
m1array=zeros(1,na+nb+2);
m2array=zeros(1,na+nb+2);
for i=1:na+1
    m1array(i)=m11(1+mod(i-1,4));
    m2array(i)=m21(1+mod(i-1,4));
end    
for i=na+2:na+nb+2
    m1array(i)=m12(1+mod(i-na-2,4));
    m2array(i)=m22(1+mod(i-na-2,4));
end    

% % % aquí viene lo bueno!!!!!
for k=1:length(w)
    a=real(Hdata(k));
    b=imag(Hdata(k));
    cdata=[a,b];
    revcdata=fliplr(cdata);
    generalA(1,:)=[w(k).^[0:length(A)-1] , w(k).^[0:length(B)-1]].*m1array;
    generalA(1+1,:)=[w(k).^[0:length(A)-1] , w(k).^[0:length(B)-1]].*m2array;
    for m=1:na+nb+2
        generalA(1,m)=cdata(1+mod(m-1,2))*generalA(1,m);
        generalA(1+1,m)=revcdata(1+mod(m-1,2))*generalA(1+1,m);
    end
    generalAw=generalA*sensMatrix;
    generalB=[-a*real(polyval(A0,jw(k)))+b*imag(polyval(A0,jw(k)))+real(polyval(B0,jw(k))) ; -b*real(polyval(A0,jw(k)))-a*imag(polyval(A0,jw(k)))+imag(polyval(B0,jw(k)))];
    e=generalA'*inv(generalA*generalA')*generalB;
    E(:,k)=e;
end
Emax=max([E';zeros(1,na+nb+2)]);
Emin=min([E';zeros(1,na+nb+2)]);

n=B0-max(Bdiff);
N=B0+max(Bdiff);
d=A0-max(Adiff);
D=A0+max(Adiff);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PLOTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[m,a]=bode(B0,A0,w);
[mdalt,adalt]=bode(N,D,w);
[mbaix,abaix]=bode(n,d,w);
figure
subplot(2,1,1)
hold on
semilogx(20*log10(m),'b--')
semilogx(20*log10(mdalt),'k')
semilogx(20*log10(mbaix),'k')
%semilogx(20*log10(abs(Hdata)),'r')
grid
% title('Mòdul');
% ylabel('dB');
% legend('Model nominal','Model intervalar','Model intervalar')
subplot(2,1,2)
hold on
semilogx(a+[360 360 360 360 360 0 0 0 0 0 0 0 0 0 0]','b--')
semilogx(adalt+[360 360 360 360 360 0 0 0 0 0 0 0 0 0 0]','k')
semilogx(abaix+[360 360 360 360 360 0 0 0 0 0 0 0 0 0 0]','k')
%semilogx(180/pi*angle(Hdata),'r')
grid
% title('Angle');
% ylabel('Graus');
% legend('Model nominal','Model intervalar','Model intervalar')
end