function f=fcostimag(x,Maximise,Norder,w)
warning off
num=x(1:Norder+1);
den=x(Norder+2:end);
[re,im]=nyquist(num,den,w);
f=(-1)^Maximise*im;

