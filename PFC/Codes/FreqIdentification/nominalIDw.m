function [ B,A,weights ] = nominalIDw( H,f,nb,na,Wt )
w=2*pi.*f;
[Bini,Aini] = invfreqs(H,w,nb,na,Wt);
A = Aini;
weights=Wt;
jw = (sqrt(-1)).*w;
for i=1:10
    Ajw = polyval(A,jw).';
    Wt = 1./abs(H.')./abs(Ajw);
    weights=[weights;Wt'];
    [B,A] = invfreqs(H,w,nb,na,Wt);
end
end