function y=degree(x)
% Aquesta �s una funci� que retorna el grau del polinomi
% tractat quan li �s donat el seu vector de coeficients.

format long e
y=size(x,2)-1;