clear all;
what=input('Pitch o Roll? \n');
if what==1 %11
    load 'p01r1.mat'; velo = 5; index=1;      
elseif what==2 %22
    load 'r01r1.mat'; velo = 6; index=2;
end
tall=200;
Angle=matriu(tall:length(navigation(:,velo)),index);
V=navigation(tall:end,velo); V = V';
f=linspace(.1,2,15);
[EReal,EImag] = DiscreteFourierTransform(Angle,f,.1,1,1,'blackmanharris');
Fe=EReal+sqrt(-1).*EImag;
[SReal,SImag] = DiscreteFourierTransform(V,f,.1,1,1,'blackmanharris');
Fs=SReal+sqrt(-1).*SImag;
Hreals=Fs./Fe; %Dades Frequencials
nb = 2; na = 2;
[H,B,A,w] = identifica(Hreals,f,na,nb);
jw = (sqrt(-1))*w;
orig = horzcat(B,A);p=zeros(length(jw),na+nb+2);
Hini = Hreals; Bini = B; Aini = A;
for i=1:length(jw)
    A = fliplr(A); B = fliplr(B);
    den = polyval(A,jw(i));
    num = polyval(B,jw(i));
    H(i) = num/den;
    [H,B,A,w] = identifica(H,f,na,nb);
    p(i,:) = horzcat(B,A);
    deriv(i,:) = abs(orig-p(i,:));
    H = Hini; B = Bini; A = Aini;
end
weights(1:(na+nb+2)) = (1/length(jw))*sum(deriv);
Dades = [real(Hreals(:)),imag(Hreals(:))];
n = horzcat(p(1,1:nb+1),[0 0 0 0 0 0]); d = horzcat(p(1,nb+2:nb+na+2),[0 0 0 0 0 0]);
Weights = diag(horzcat(weights(nb+2:nb+na+2),weights(1:nb+1)));
for k=1:length(jw)
    a = Dades(k,1); b = Dades(k,2); wi =w(k);
    A1 = [a -b*wi -a*(wi^2) b*(wi^3) -a*(wi^4) b*(wi^5) a*(wi^6);
          b a*wi -b*(wi)^2 -a*(wi^3) b*(wi^4) a*(wi^5) -b*(wi^6)];
    A2 = [-1 0 (wi)^2 0 -(wi^4) 0 (wi^6);
          0 wi 0 -(wi^3) 0 (wi^5) 0];
    A1 = A1(:,1:nb+1); A2 = A2(:,1:na+1);
    Amatrix = horzcat(A1,A2);
    Bmatrix = [-a*(d(1)-d(3)*(wi^2) + d(5)*(wi^4)-d(7)*(wi^6))+b*(d(2)*wi-d(4)*(wi^3)+d(6)*(wi^5))+(n(1)-n(3)*(wi^2)+n(5)*(wi^4)-n(7)*(wi^6));
         -b*(d(1)-d(3)*(wi^2)+d(5)*(wi^4)-d(7)*(wi^6))-a*(d(2)*wi-d(4)*(wi^3)+d(6)*(wi^5))+n(2)*wi-n(4)*(wi^3)+n(6)*(wi^5)];
    aux = transpose(Amatrix*Weights);
    aux2 = (Amatrix*Weights)*(transpose(Amatrix*Weights));
    aux2 = aux2^(-1);
    aux3 = aux*aux2;
    epsi(k,:) = (aux3)*Bmatrix;
end
n = n(1:nb+1); d = d(1:na+1);
epsilon=zeros(2,nb+na+2);
for m=1:length(epsilon(1,:))
    epsilon(1,m)=min(epsi(:,m));epsilon(1,m)=min(0,epsilon(1,m));
    epsilon(2,m)=max(epsi(:,m));epsilon(2,m)=max(0,epsilon(2,m));
end    
epsilon=horzcat(epsilon(:,na+2:nb+na+2),epsilon(:,1:na+1));
for l=1:length(epsilon(1,:))
        intervalars(1,l)=p(1,l)+weights(l)*epsilon(1,l);
        intervalars(2,l)=p(1,l)+weights(l)*epsilon(2,l);
end
Bd=intervalars(2,1:3);Ad=intervalars(2,4:6);Bb=intervalars(1,1:3);Ab=intervalars(1,4:6);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PLOTS %%%%%%%%%%%%%%%%%%%%%%
len = 5;
for l=1:length(epsilon(1,:))
    minim = min(intervalars(:,l));maxim = max(intervalars(:,l));
    valors(:,l) = linspace(minim,maxim,len);
end
%%%%%%%%
% if what==1
%     Bbaix = horzcat(valors(1,1),100,valors(2,3)); Abaix = valors(1,4:6);
%     Bdalt = horzcat(valors(3,1),5000,50000); Adalt = horzcat(1,-1.98,valors(4,6));
%     [m,a]=bode(Bbaix,Abaix,w);[m2,a2]=bode(Bdalt,Adalt,w);
%     angle1=a+[360 360 360 360 0 360 0 0 0 0 0 0 0 0 0]';
%     angle2=a2+[360 360 360 360 0 360 0 0 0 0 0 0 0 0 0]';
%     save('pitch','Bdalt','Adalt','Bbaix','Abaix','Hreals')
% elseif what==2
%     %Bdalt = valors(5,1:3);Adalt=valors(5,4:7);Bbaix=valors(1,1:3);Abaix=valors(1,4:7);
%     %Bdalt = horzcat(valors(1,1:2),20e3,20e3); Adalt = horzcat(valors(4,5:8));%ordre 3 3
%     Bdalt = horzcat(valors(2,1:3)); Adalt=horzcat(valors(3,4:5),valors(5,6)); %ordre 2 2
%     %Bbaix = horzcat(valors(1,1:3),valors(3,4)); Abaix = valors(1,5:8);%ordre 3 3
%     Bbaix = horzcat(valors(5,1),2500,valors(5,3)); Abaix=horzcat(valors(3,4:5),valors(5,6)); %ordre 2 2
%     [m,a]=bode(Bbaix,Abaix,w);[m2,a2]=bode(Bdalt,Adalt,w);
%     angle1=a+[360 360 360 360 360 360 360 360 360 0 0 0 0 0 0]';
%     angle2=a2+[360 360 360 360 360 360 360 360 360 0 0 0 0 0 0]';
%     save('roll','Bdalt','Adalt','Bbaix','Abaix','Hreals')
% end
figure
subplot(2,1,1)
hold on
semilogx(m,'b--')
semilogx(m2,'b--')
semilogx(abs(Hini),'r')
grid
title('Mòdul');
ylabel('dB');
legend('Ajust1','Ajust2','Dades');
subplot(2,1,2)
hold on
semilogx(angle1,'b--')
semilogx(angle2,'b--')
semilogx(180/pi*angle(Hini),'r')
grid
title('Angle');
ylabel('degrees');
legend('Ajust1','Ajust2','Dades');
