function [y,rmat]=routh(x);

%
% Aquesta �s una funci� que aplica el test de Routh
% sobre un polinomi expressat pel seu vector de coeficients x 
% de manera que retorna un missatge amb el nombre de zeros que
% el polinomi t� amb part real positiva.
%
% Aquesta funci� �s capa� de tractar tant el cas singular 1 com
% el cas singular 2 sense cap mena de problema.
% L'�nica exig�ncia rau en proporcionar el vector x de
% coeficients de manera completa i ordenada de manera decreixent
% des del grau del polinomi fins al terme independent.
%
%
% Per exemple, si s'escriu:
%
% 		y=routh([-5 4 -3 2 1])
%
% La funci� retorna:
%
%		y=3
%
% N�mero que coincideix amb el nombre de zeros que el polinomi t�
% al semipl� Re(z)>0.
%

format long e

vec1=[];
if x(1)<0
x=(-1)*x;
end
deg=degree(x);
i=1;
while i<=deg+1
	vec1=[vec1,x(:,i)];
	i=i+2;
end
vec2=zeros(1,size(vec1,2));
j=2;
while j<=deg+1
	vec2(:,(j/2))=x(:,j);
	j=j+2;
end

if vec2==zeros(1,size(vec2,2))
	for k=1:size(vec2,2),
		vec2(k)=vec1(k)*(deg-((k-1)*2));
	end
end
if vec2(1)==0
	vec2(1)=0.000000000001;
end
rmat=[vec1;vec2];
ind=3;
while size(rmat,1)<=deg
	ia=ind-2;
	ib=ind-1;
	uu=rmat(ib,1);
	u=rmat(ia,1);
	r=u/uu;
	raux=rmat(ia,:)-r*rmat(ib,:);
	raux=rota(raux,1);
	
	if raux==zeros(1,size(raux,2))
		for j=1:size(rmat,2),
			raux(j)=rmat(ib,j)*((deg-ind+2)-((j-1)*2));
		end
	end	

	if raux(:,1)==0
		raux(:,1)=0.000000000001;
	end
	
	rmat=[rmat;raux];
	ind=ind+1;
end
rmat=nor(rmat);
detect=size(rmat,1)-1;
sumglo=0;
vecpro=[];
for i=1:detect,
	op=rmat(i,1)*rmat(i+1,1);
	vecpro=[vecpro,op];
	suma=sum(vecpro);
end
y=round((deg-suma)/2);
end

