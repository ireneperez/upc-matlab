function [A1,A2,A3,A4]=kharitonov(Am,Ap)
% 	KHARITONOV  Determine four Kharitonov vertices.
%     	[A1,A2,A3,A4]=kharitonov(Am,Ap) computes the four Kharitonov polynomials from
%     	A(jw)=\sum_{k=0}^{n}{[Am_k,Ap_k](jw)^{k}}
% 	Index determination A1, A2, A3, A4

A=[Am;Ap];
I=[1 1 2 2;...
2 2 1 1;...
1 2 2 1;...
2 1 1 2];
N=mod(size(A,2),4);
for i=1:N+1
    I=[I,I];
end
differ=size(I,2)-size(A,2);
I(:,1:differ)=[];    

%% Four vertices definition 
for col=1:size(A,2)
A1(col)=A(I(1,col),col);
A2(col)=A(I(2,col),col);
A3(col)=A(I(3,col),col);
A4(col)=A(I(4,col),col);
end
