function f = fcost(x,maximHR,minimHR,maximHI,minimHI,w,OrdreNum,OrdreDen)
Bmin=x(1:OrdreNum+1);
Bmax=x(OrdreNum+2:OrdreNum*2+2);
Amin=x(OrdreNum*2+3:OrdreNum*2+3+OrdreDen);
Amax=x(OrdreNum*2+3+OrdreDen+1:end);
[MaxReal,MinReal,MaxImag,MinImag,ModuleMax,ModuleMin,AngleMax,AngleMin]=IntervalModelFrequencyResponse(Bmin,Bmax,Amin,Amax,w);
faux=zeros(1,length(maximHR));
maximHR = maximHR'; minimHR = minimHR';maximHI=maximHI';minimHI=minimHI';
for i =1:length(faux)
 faux(i) = abs(MaxReal(i)-maximHR(i))^2+(MinReal(i)-minimHR(i))^2+(MaxImag(i)-maximHI(i))^2+(MinImag(i)-minimHI(i))^2;
end
f=sqrt(sum(faux))