function f = atan2dmod(y,x)
% Function calculates the arc tangent of y/x and placees the result in
% range of [0..360]
f=zeros(length(y),1);
for i=1:length(y)
    f(i)=atan2d(y(i),x(i));
    while f(i)>0
        f(i)=f(i)-360;
    end
end
