function [c,ceq] = restriccions(x,maximHR,minimHR,maximHI,minimHI,w,OrdreNum,OrdreDen)
Bmin=x(1:OrdreNum+1);
Bmax=x(OrdreNum+2:OrdreNum*2+2);
Amin=x(OrdreNum*2+3:OrdreNum*2+3+OrdreDen);
Amax=x(OrdreNum*2+3+OrdreDen+1:end);
[MaxReal,MinReal,MaxImag,MinImag,ModuleMax,ModuleMin,AngleMax,AngleMin]=IntervalModelFrequencyResponse(Bmin,Bmax,Amin,Amax,w);
c = [maximHR'-MaxReal,MinReal-minimHR',maximHI'-MaxImag,MinImag-minimHI',Bmin-Bmax,Amin-Amax]'
ceq = [];