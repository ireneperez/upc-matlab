clear all
close all
global imageSUB navdataSUB cmd_velPUB geometryMSG;
%% ....Reseteig de l'ARDrone per si no ho està (INACTIU: EL SEND ESTÀ COMENTAT!!!!)....
% Creació del publicador per a resetejar l'ARDrone
resetPUB = rospublisher('/ardrone/reset');
magnetPUB = rospublisher('/ardrone/mag');
% Creació d'un missatge "Empty" que es farà servir per a molts topics. En
% l'ARDrone es fa servir per a topics que només tenen una consigna.
emptyMSG = rosmessage('std_msgs/Empty');
magMSG = rosmessage('geometry_msgs/Vector3Stamped');
% Crea el client del servei flattrim (fa un zero dels sensors) 
flattrimCLI = rossvcclient('/ardrone/flattrim');
%% ....Dades (Navdata)....
% Creació del subscriptor de Navdata
navdataSUB = rossubscriber('/ardrone/navdata');
%% ....Camera....
% Crea clients del serveis necessaris per a selecconar la camera Zenital
setCamFrontCLI = rossvcclient('/ardrone/setcamchannel');
toggleCamCLI = rossvcclient('/ardrone/togglecam');
% Crida els serveis de selecció de càmera.
% No s'ha trobat un sistema per a seleccionar la camera zenital
% directament, el mètode que es fa servir és:
call(setCamFrontCLI);
% que selecciona sempre la càmera frontal i:
call(toggleCamCLI);
pause(0.8);
% que commuta entre les dues cameres.
% Fi de la inicialització
%% ....Inici del Main....
% Crea els publicadors de control
landPUB = rospublisher('/ardrone/land');
takeOffPUB = rospublisher('/ardrone/takeoff');
cmd_velPUB = rospublisher('cmd_vel');
imageSUB = rossubscriber('/ardrone/image_raw/compressed');
geometryMSG = rosmessage('geometry_msgs/Twist');
geometryMSG.Linear.X= 0;
geometryMSG.Linear.Y= 0;
geometryMSG.Linear.Z= 0;
geometryMSG.Angular.X= 0;
geometryMSG.Angular.Y= 0;
geometryMSG.Angular.Z= 0;
% Envia el missatge de comanda de moviment
send(cmd_velPUB,geometryMSG);
% Envia el miaastge d'enlairament
send(takeOffPUB,emptyMSG);
period = 1;
temporitzador = timer('TimerFcn',@rtimer,'Period',period,'ExecutionMode','fixedRate');
disp('Pulsa return per iniciar, o fletxes per guiar');
start(temporitzador);
disp('Pulsa return per finalitzar');
input('')
stop(temporitzador);
delete(temporitzador);
pause(0.5);
send(landPUB,emptyMSG);
