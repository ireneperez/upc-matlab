function [vector] = Control(key,mostra)
%Suposarem que la velocitat només afecta pel paràmetre de Roll, no afectarà
%per tant ni a Pitch ni a Yaw.
SP_Roll(:,mostra)=0; SP_Pitch=0; SP_Yaw=0;
    if mostra<=3    
        SP_Roll(:,mostra)=0; SP_Pitch=0; SP_Yaw=0;
    else
            if key==30 %Pitch positiu
                SP_Pitch = 0.1;
            elseif key==31 %Pitch negatiu
                SP_Pitch = -0.1;
            elseif key==28 %Roll positiu
                SP_Roll(mostra) = 0.1;
            elseif key==29 %Roll negatiu
                SP_Roll(mostra) = -0.1;
            elseif key==114 %Yaw cap a la dreta
                SP_Yaw = 0.5;
            elseif key==108 %Yaw cap a l'esquerra
                SP_Yaw = -0.5;  
            end     
    end
    SP_Gaz = 0; 
    vector = [SP_Pitch, SP_Roll(mostra), SP_Yaw, SP_Gaz];
end

