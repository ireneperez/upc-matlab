function [] = rtimer( src,~ )
global imageSUB navdataSUB cmd_velPUB geometryMSG;
mostra = src.TasksExecuted;
disp(mostra);
key = getkey;
disp(key);
%im2read = receive(imageSUB);
[vector] = Control(key,mostra);
SP_Pitch = vector(1); SP_Roll(mostra) = vector(2); 
SP_Yaw = vector(3); SP_Gaz = vector(4);
angularX = 0; angularY = 0;
disp(vector); 
%Omplir les variables del missatge de moviment
    geometryMSG.Linear.X =SP_Pitch;
    geometryMSG.Linear.Y =SP_Roll(mostra);
    geometryMSG.Linear.Z =SP_Gaz;
    geometryMSG.Angular.X=angularX;
    geometryMSG.Angular.Y=angularY;
    geometryMSG.Angular.Z=SP_Yaw;       
%Enviar la comanda de moviment
send(cmd_velPUB,geometryMSG);
end

