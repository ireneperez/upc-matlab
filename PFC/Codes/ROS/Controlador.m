function [PV_X, MV_X, SP_Pitch,SP_Roll,SP_Yaw,SP_Gaz] = Controlador(mostra,linesH,navdata,PV_X,MV_X,SP_Roll)
%Suposarem que la velocitat només afecta pel paràmetre de Roll, no afectarà
%per tant ni a Pitch ni a Yaw.
PV_Vx(:,mostra)=navdata.Vy; SP_X = 270; PitchFactor = 0.01; Tm = 0.12;
    if mostra<=3    
        SP_Roll(:,mostra)=0; PV_X(:,mostra)=0; SP_Pitch=0; SP_Yaw=0;
    else
        l=exist('linesH','var');
        if l==0
            PV_X(:,mostra)=0; SP_Pitch=0; SP_Yaw=0;
        elseif isempty(linesH)==1
            PV_X(:,mostra)=PV_X(mostra-1); SP_Pitch=0; SP_Yaw=0;
        else
            pointXY1=linesH.point1; pointXY2=linesH.point2; PV_X(:,mostra)=pointXY1(1);
            SP_Yaw=0.0037*(pointXY2(1)-pointXY1(1));
            if pointXY1(1)>220 || pointXY1(1)<320
                SP_Pitch=PitchFactor;
            else
                SP_Pitch=PitchFactor*0.5;
            end
        end
    end
Kp_X=3.031506559915031e-04; Ki_X=9.855172928142329e-05; Kd_X=2.331271122743291e-04; Kp_Vx=6.704983097149473e-04; Ki_Vx=4.839273835675924e-04*Tm;
    if mostra<3 
        MV_X(:,mostra)=Kp_X*(SP_X-PV_X(mostra));
    else
        MV_X(:,mostra)=MV_X(:,mostra-1)+Kp_X*((SP_X-PV_X(mostra))-(SP_X-PV_X(mostra-1)))+Ki_X*(SP_X-PV_X(mostra))+Kd_X*(PV_X(mostra-1)-PV_X(mostra));
        SP_Vx(:,mostra)=MV_X(mostra);
        SP_Roll(:,mostra)=SP_Roll(:,mostra-1)+Kp_Vx*(SP_Vx(mostra)-PV_Vx(mostra))-Kp_Vx*(SP_Vx(mostra-1)-PV_Vx(mostra-1))+Ki_Vx*(SP_Vx(mostra)-PV_Vx(mostra));
            if      (SP_Roll(mostra)>1); SP_Roll(mostra)=1;  
            elseif  (SP_Roll(mostra)<-1); SP_Roll(mostra)=-1;
            end
    end
    SP_Gaz = 0; 
disp(PV_X);
end

