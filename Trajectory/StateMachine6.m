close all
clear all
global imageSUB navdataSUB cmd_velPUB imuSUB geometryMSG;
action=0;
clc
uiwait(msgbox('Waiting for the user. AR-DRONE is LANDED. Ready for taking off!','Oooops....','warn'))

% % % % % while (action~=115 && action~=83)%MENTRE NO ES PREMI S/s
% % % % %    disp('Press s/S to start (take off and hovering the AR-DRONE)'); 
% % % % %    action=getkey(1);
% % % % % end
action=0;
% % % % donar ordre enlairament
setCamFrontCLI = rossvcclient('/ardrone/setcamchannel');
toggleCamCLI = rossvcclient('/ardrone/togglecam');
call(setCamFrontCLI);
call(toggleCamCLI);
emptyMSG = rosmessage('std_msgs/Empty');
resetPUB = rospublisher('/ardrone/reset');
% % % % publicacions
landPUB = rospublisher('/ardrone/land');
takeOffPUB = rospublisher('/ardrone/takeoff');
cmd_velPUB = rospublisher('cmd_vel');
% % % % Suscripcions
imageSUB = rossubscriber('/ardrone/image_raw/compressed');
navdataSUB = rossubscriber('/ardrone/navdata');
imuSUB = rossubscriber('/ardrone/imu/');
% % % % posar totes les consignes a 0, per a l'inici....
geometryMSG = rosmessage('geometry_msgs/Twist');
geometryMSG.Linear.X= 0;
geometryMSG.Linear.Y= 0;
geometryMSG.Linear.Z= 0;
geometryMSG.Angular.X= 0;
geometryMSG.Angular.Y= 0;
geometryMSG.Angular.Z= 0;
send(cmd_velPUB,geometryMSG);
% Envia el missatge d'enlairament
send(takeOffPUB,emptyMSG);
% % % Esperar que l'aeronau estigui en HOVERING---> hi ha manera de
% saber-ho a través de les suscripcions
navdata=receive(navdataSUB);
while (navdata.State~=4)            %esperem a que ens digui que estem en hovering....
    navdata=receive(navdataSUB);
    pause(1)
end

uiwait(msgbox('Waiting for the user. AR-DRONE is HOVERING. Ready for the move!','Oooops....','warn'))
% figure

SP = 1;
while (action~=111 && action~=79)%mentre no sigui o/O
        while (action~=113 && action~=81)%mentre no sigui q/Q
        %     clc
           disp('Press KEYBOARD ARROWS to move FORWARD/BACKWARD (X direction) or LEFT/RIGHT (Y direction)'); 
           disp('Press r/R or l/L to rotate CLOCKWISE/COUNTERCLOCKWISE');
           disp('Press q/Q to QUIT (landing)'); 
           action=getkey(1);
           if (action==30)
               disp('Moving FORWARD along the X direction')
               geometryMSG.Linear.X = SP;  
           elseif (action==31)
               disp('Moving BACKWARDS along the X direction')
               geometryMSG.Linear.X = -SP;     
           elseif (action==28)
               disp('Moving LEFT along the Y direction')
               geometryMSG.Linear.Y = SP; 
           elseif (action==29)
               disp('Moving RIGHT along the Y direction')
               geometryMSG.Linear.Y = -SP; 
           elseif (action==76 || action==108)
               disp('Rotating LEFT along the Z direction')
               geometryMSG.Angular.Z = SP;
           elseif (action==82 || action==114)
               disp('Rotating RIGHT along the Z direction')
               geometryMSG.Angular.Z = -SP;
           elseif action==117
               geometryMSG.Linear.Z = SP;
           elseif action==100
               geometryMSG.Linear.Z = -SP;
           else
               disp('not moving')  
           end
            send(cmd_velPUB,geometryMSG);
            disp('Battery percentage:');
            disp(navdata.BatteryPercent);
            pause(.2);
            geometryMSG.Linear.X= 0;
            geometryMSG.Linear.Y= 0;
            geometryMSG.Linear.Z= 0;
            geometryMSG.Angular.X= 0;
            geometryMSG.Angular.Y= 0;
            geometryMSG.Angular.Z= 0;
                send(cmd_velPUB,geometryMSG);

        end
        clc
        disp('Pressiona o/O per OK i n/N per NOK')
        im2read=receive(imageSUB);
        imatge=readImage(im2read);
        imshow(imatge)
        action=getkey(1);
end

close all
close all hidden
period=.1;
temporitzador = timer('TimerFcn',@rtimer6,'Period',period,'ExecutionMode','fixedRate');
disp('Pulsa return per iniciar');
start(temporitzador);
disp('Pulsa return per finalitzar');
input('')
stop(temporitzador);
delete(temporitzador);
pause(0.5);
send(landPUB,emptyMSG);


% % % Alternativament, es pot construir la màquina d'estats 