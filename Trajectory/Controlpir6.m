function [vector,MV_X,MV_Y,MV_Z,errorX,errorY,errorZ,accio,flag,GirZ,GirEfectuat,ESTAT] = Controlpir6(mostra,vector,MV_X,MV_Y,MV_Z,errorX,errorY,errorZ,navdata,imudata,accio,linesV,linesH,flag,GirZ,GirEfectuat,ESTAT)
%%%%%% Paràmetres de la càmara %%%%%%
% disp(flag)
% disp(navdata.RotZ)
% PitchFactor=250;%mm/s
PitchFactor=300;%mm/s
% RollFactor=150;%mm/s 
RollFactor=150;%mm/s 
SP_X = 320; %mm
%%% Per fer el seguiment de línia els setpoints no venen d'una matriu,
%%% han de venir del tractament d'imatge


%%%%%% Dades del controlador de pitch %%%%%%%
Tm = 0.1;
KpaX = 0.00025; KiaX = 0.00005; KiX = KiaX*Tm;%alerta...hi havia KpaX = 0.00025;
%%%%%% Dades del controlador de roll %%%%%%%
KpaY = 0.00035; KiaY = 0.00008; KiY = KiaY*Tm;
%%%%%% Dades del controlador de yaw %%%%%%%
KpaZ = 0.00025; KiaZ = 0.00005; KiZ = KiaZ*Tm;
% KpaZ = -0.0381; KiaZ = 0.0210; KiZ = KiaZ*Tm;

%%%%%% Dades heredades %%%%%%%
MV_Xant=MV_X; errorantX=errorX; MV_Yant=MV_Y; errorantY = errorY; MV_Zant=MV_Z; errorantZ = errorZ;
PV_Vx=navdata.Vx; PV_Vy=navdata.Vy; PV_Vz=imudata.AngularVelocity.Z; PV_Vz=PV_Vz*1000;
% % % % condicions per a les transicions d'estat
    if (((isempty(linesH) && isempty(linesV)) && ESTAT~=2) )%|| (isempty(linesV) && ESTAT~=0)
        ESTAT=0; % fent hovering
    elseif (ESTAT==0 && ~isempty(linesV)) %|| (ESTAT==1 && ~isempty(linesV)) 
        ESTAT=1; % seguint línia
    elseif (ESTAT==1 && ~isempty(linesH))
        GirZ=navdata.RotZ;
        marcaDeGir=navdata.RotZ;
        ESTAT=2; % efectuant gir de 90º
    elseif (ESTAT==2 && GirEfectuat )%&& ~isempty(linesV)
        ESTAT=3; % escapant de la cruïlla
        GirEfectuat=0;
    elseif (ESTAT==3 && isempty(linesH))
        ESTAT=1;
%     else
%         ESTAT=0;
    end
    fprintf('l''estat és %i\n',ESTAT)
% % % %  fi de les condicions per a les transicions d'estat

% % % % inici de les accions de cada estat
    
    if ESTAT==0
        SP_Vx=0;SP_Vy=0;SP_Vz=0;SP_Gaz=0; % no estic segur d'això....
%         flag = 0;
    elseif ESTAT==1
            dv=linesV.point1; dv2 = linesV.point2;
            %SP_Yaw = KpaZ*(dv2(1)-dv(1));
            x = linesV.rho; theta = linesV.theta; Vx = PV_Vx;
            SP_Vz = trajectoria(x,theta,Vx);
            if dv(1)>(SP_X-50) && dv(1)<(SP_X+50)
                SP_Vx=PitchFactor;    
                SP_Vy=vector(2);
            elseif dv(1)<=(SP_X-50) 
                SP_Vx=PitchFactor/2;
                SP_Vy=RollFactor;
            elseif dv(1)>=(SP_X+50) 
                SP_Vx=PitchFactor/2;
                SP_Vy=-RollFactor; %Comprovats els signes de roll!!
            end
            SP_Gaz=0;
    elseif ESTAT==2
                lectura=navdata.RotZ;
                    disp(lectura-GirZ)
                    if ((lectura-GirZ>=-90) && (lectura-GirZ<=50)) || (lectura-GirZ>=280) 
                      [SP_Vx,SP_Vy,SP_Vz,SP_Gaz] = Gir6(); 
                      disp(lectura-GirZ)
                   else
                      GirEfectuat=1;
                     %SP_Roll=0; SP_Pitch=0; SP_Yaw=0; SP_Gaz = 0;
                     SP_Vx=0;SP_Vy=0;SP_Vz=0;SP_Gaz=0; % no estic segur d'això....
                   end
                   
    elseif ESTAT==3
%                     pause(.5)
                    if isempty(linesV)
                            SP_Vx=0;SP_Vy=0;SP_Vz=0;SP_Gaz=0; % no estic segur d'això....
                    else
                        dv=linesV.point1; dv2 = linesV.point2;
                        %SP_Yaw = KpaZ*(dv2(1)-dv(1));
                        x = linesV.rho; theta = linesV.theta; Vx = PV_Vx;
                        SP_Vz = trajectoria(x,theta,Vx);
                        if dv(1)>(SP_X-50) && dv(1)<(SP_X+50)
                            SP_Vx=PitchFactor*3;    
                            SP_Vy=vector(2);
                        elseif dv(1)<=(SP_X-50) 
                            SP_Vx=PitchFactor;%/2;
                            SP_Vy=RollFactor;
                        elseif dv(1)>=(SP_X+50) 
                            SP_Vx=PitchFactor;%/2;
                            SP_Vy=-RollFactor; %Comprovats els signes de roll!!
                        end
                        SP_Gaz=0;         
                    end
    end
        PV_Vz = PV_Vz/1000;   
        errorY=SP_Vy-PV_Vy; errorX=SP_Vx-PV_Vx;errorZ=SP_Vz-PV_Vz;
        MV_X = MV_Xant + KpaX*(errorX-errorantX)+KiX*errorX;        
        if MV_X>1; MV_X=1; elseif MV_X<-1; MV_X=-1; end
        MV_Y = MV_Yant + KpaY*(errorY-errorantY)+KiY*errorY;        
        if MV_Y>1; MV_Y=1; elseif MV_Y<-1; MV_Y=-1; end
        MV_Z= MV_Zant + KpaZ*(errorZ-errorantZ)+KiZ*errorZ;        
        if MV_Z>1; MV_Z=1; elseif MV_Z<-1; MV_Z=-1; end
        SP_Pitch = MV_X; SP_Roll = MV_Y; SP_Yaw = MV_Z;

        vector = [SP_Pitch, SP_Roll, SP_Yaw, SP_Gaz];
    accio(mostra,:) = vector;
end