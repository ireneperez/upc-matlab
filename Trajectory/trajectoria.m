function [omega] = trajectoria(x,theta,Vx)
L = 0.25; %distancia de recuperacio en metres
%L ha de complir que L/TV >= 1
L = 0.1*Vx;
gamma = (x*cos(theta))-(sqrt(L^2 - x^2))*sin(theta);
gamma = (2/L^2)*gamma;
omega = Vx*gamma;
end

