function [linesH,linesV] = treatimages6(imreceived)

image = readImage(imreceived);
imageYCBCR = rgb2ycbcr(image);
imageCR = imageYCBCR(:,:,2);
level = 0.70;
imageBW = im2bw(imageCR,level);

%%%%%%%%%%%%%% HORIZONTAL %%%%%%%%%%%%%%%
liniaH= strel('line',50,0);
imagelineH = imerode(imageBW,liniaH);
imagemorphH = bwmorph(imagelineH,'skel',Inf);
[h, theta, rho] = hough(imagemorphH,'Theta',-90:89);
peak=houghpeaks(h,1);
linesH = houghlines(imagemorphH,theta,rho,peak);

%%%%%%%%%%%%%% VERTICAL %%%%%%%%%%%%%%%%
liniaV= strel('line',50,90);
imagelineV = imerode(imageBW,liniaV);
imagemorphV = bwmorph(imagelineV,'skel',2);
[hV, thetaV, rhoV] = hough(imagemorphV,'Theta',-45:45);
peakV=houghpeaks(hV,1);
linesV = houghlines(imagemorphV,thetaV,rhoV,peakV);

% figure
% subplot(2,2,1); 
% imshow(image);
% subplot(2,2,2);
% imshow(imagemorphH);
% subplot(2,2,3);
% imshow(imagemorphV);

%HAurem de revisar quines possibilitats de distancia i angle negatius i
%positius tenim amb el drone
if isempty(linesH) == 1
    if isempty(linesV)==1
        disp('No detecta res')
    else
        disp('Vertical');%dv = linesV.rho; disp(dv(1));
    end
else
    if isempty(linesV)==1
        disp('Horitzontal')
    else
        disp('Cruilla');
    end
end
end