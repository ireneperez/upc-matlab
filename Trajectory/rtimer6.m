function [] = rtimer6( src,~ )
persistent vector MV_X MV_Y MV_Z errorX errorY errorZ flag GirZ GirEfectuat ESTAT;
global navdataSUB imageSUB cmd_velPUB geometryMSG imuSUB navigation accio;
mostra = src.TasksExecuted;%disp(mostra);
vector=[0 0 0 0];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
imreceived = receive(imageSUB);
[linesH,linesV] = treatimages6(imreceived);

navdata = receive(navdataSUB); imudata = receive(imuSUB);
if mostra==1
    MV_X = 0; MV_Y = 0; MV_Z = 0; errorX=0; errorY = 0; errorZ = 0;
    GirEfectuat=0;
    ESTAT=0;
end
[vector,MV_X,MV_Y,MV_Z,errorX,errorY,errorZ,accio,flag,GirZ,GirEfectuat,ESTAT] = Controlpir6(mostra,vector,MV_X,MV_Y,MV_Z,errorX,errorY,errorZ,navdata,imudata,accio,linesV,linesH,flag,GirZ,GirEfectuat,ESTAT);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

SP_Pitch = vector(1); SP_Roll = vector(2); 
SP_Yaw = vector(3); SP_Gaz = vector(4);
angularX = 0; angularY = 0;
    geometryMSG.Linear.X =SP_Pitch;
    geometryMSG.Linear.Y =SP_Roll;
    geometryMSG.Linear.Z =SP_Gaz;
    geometryMSG.Angular.X=angularX;
    geometryMSG.Angular.Y=angularY;
    geometryMSG.Angular.Z=SP_Yaw;       
send(cmd_velPUB,geometryMSG);
navdata = receive(navdataSUB);
imudata = receive(imuSUB);
omega =imudata.AngularVelocity.Z;
omega = omega*1000000;
nav = [navdata.RotX,navdata.RotY,navdata.RotZ,navdata.Altd,navdata.Vx,navdata.Vy,omega];
navigation(mostra,:) = nav;
end